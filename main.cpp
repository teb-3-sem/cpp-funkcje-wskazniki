#include <iostream>
//biblioteka niezbedna do operacji matematycznych
#include <cmath>
#include <string>

using namespace std;

//definicja własnego typu - unsigned int będzie mogło być wywoływane poprzez krótkie uint
typedef unsigned int uint;
//inna definicja typu - poprzez nazwę ullong będzie można wywołać pierwotny typ unsigned long long
using ullong = unsigned long long;

/*
 * Przykład tworzenia własnych funkcji w języku C++; funkją jest także main, jednak tamta funkcja
 * MUSI isatnieć w programie. Inne funkcje możemy sami tworzyć, z własnymi nazwami. Funkcje mogą
 * zwracać wartości (jak chociażby main - zwraca wartość int) lub nic nie zwracać (void - jak ta poniżej)
 * Funkcje mogą (nie muszą) przyjmować argumenty; argument to nic innego zmienna, posiadający swój typ i nazwę;
 * Agrumenty mogą przyjmować domyślne wartości (poniżej przyjmuje ją wiek -> 0).
 * */
void mojaNowaFunkcja(string nazwisko, int wiek=0) {
    /*
     * Kod w funkcjach jest taki sam jak w funkcji main, można przez niego wykonywac dowolne operacje
     * */
    string imie="Maciek";
    cout << "Zapisane dane " << imie << ' ' << nazwisko;
    if (wiek!=0)
    cout << ", wiek to: "
         << wiek;

    cout << endl;
}

/*
 * Funkcja poniżej zwraca wynik jednej z 4 operacji arytmetycznej: dodawania, odejmowania, mnożenia
 * dzielenia. Znak operacji przyjmowany jest przez argument op.
 *
 * Pierwszy argument, liczby, przyjmuje całą tablicę liczb o wartościach zmiennoprzecinkowych (podwójnej
 * precyzji). Wstępnie funkjca wylicza wynik tylko z dwóch pierwszych wartości tablicy; należy zmienić
 * to na wyliczanie ze wszystkich podanych liczb (w kolejności podanej przez tablicę).
 * */
double obliczenia(double liczby[], char op) {
    //zmienna będzie zapisywać nasz wynik
    double wynik=0;
    //poniższa zmienna to tzw. zmienna wskaźnikowa. Nie przechowuje ona wartości (w tym wypadku zmiennoprzecinkowej)
    //lecz wskazanie do numeru komórki pamięci RAM, w której znajduje się PIERWSZY element w tablicy wszystkich
    //przekazanych liczb. Ta zmienna będzie służyć do przemieszczania się po talicy
    double *p= &liczby[0];
    //test zmiennej p - jeżeli wykorzystamy ją jako nazwę - zwróci adres komórki; jeżeli użyjemy gwiazdki -
    //wyświetlimy zawartość przechowywaną w komórce
    cout << *p << ' ' << p;
    //potrzebne nam jest zliczenie ilości liczb dostępnych w tablicy danych. sizeof niestety poda tylko wielkość
    //(rozmiar) zmiennej w tablicy, nie zaś sumaryczną wielkość całej tablicy.
    //cout << "tablica " << sizeof(liczby) << endl;
    //poniżej rozwiązanie zliczenia tablicy; do chwili, kiedy nasz kod nie trafi na znak null (tym zawsze kończy się
    //tablica) przeskakuje on po kolejnych elementach - liczbach - w tablicy.
    while(static_cast<char>(*p) != '\0') {
        cout << *p << ' ';
        //wskaźnik można zwiększać (inkrementacja) - przeskakuje on wtedy na kolejną komórkę w pamięci (z kolejną wartością)
        p++;
    }
    //operacje wykonywane na liczbach z tablicy
    switch (op) {
        case '+': wynik=liczby[0]+liczby[1]; break;
        case '-': wynik=liczby[0]-liczby[1]; break;
        case '*': wynik=liczby[0]*liczby[1]; break;
        case '/': wynik=liczby[0]/liczby[1]; break;
        default: cout << "Nie znak takiej operacji, koncze obliczenia!";
    }
    //UWAGA! Należy zmienić powyższy kod tak, by przeliczał np. sumowanie, po każdej możliwej wartości. Tak samo należy
    //postąpić z odejmowaniem, mnożeniem oraz dzieleniem. Dzielenie powinno także uwaględniać niemożność dzielenia przez
    //zero.
    return wynik;
}

int main()
{

    uint bezznaku=15;
    cout << bezznaku << ' ' << endl;
    cout << "Wywolanie funkcji " << endl;
    mojaNowaFunkcja("Kowlaski", 45);
    //utworzenie lokalnej tablicy czteroelementowej z liczbami do przekazania do funkcji
    double o[4] = {45,2,67,14};
    //funkcję można wywoływać także w ramach innych operacji - w tym wypadku podczas
    //wyświetlania tekstu (cout)
    cout << "45 * 2 = " << obliczenia(o,'*') << endl;
    return 0;
}
